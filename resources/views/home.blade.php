@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Accueil') }}</div>

                <div class="card-body">
                    <table>
                        <th>
                            Locations
                        </th>
                        <th>
                            Modifier
                        </th>
                        <th>
                            Finaliser
                        </th>
                        <th>
                            Supprimer
                        </th>
                        @foreach($rentals as $key => $value)
                            <tr>
                                <td>
                                    {{$value}}
                                </td>
                                <td>
                                    <a href="{{url('location/preparation/edit/' . $key)}}" style="color: darkblue">Modifier</a>
                                </td>
                                <td>
                                    <a href="{{url('location/' . $key)}}" style="color: green">Finaliser</a>
                                </td>
                                <td>
                                    <a href="{{url('location-delete/' . $key)}}" style="color: darkred">Supprimer</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="buttonHolder">
                        <a href="{{url('create-location')}}"><input class="rentalSubmit" type="submit" value="Ajouter une nouvelle location" style="background-color: #3f83f8"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
