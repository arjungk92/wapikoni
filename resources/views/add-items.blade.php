@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    @if(isset($rental))
                        <div class="card-header">Modifier la préparation de : {{$rental}}</div>
                    @else
                        <div class="card-header">Choisir les types</div>
                    @endif

                    <div class="card-body">
                        @if(isset($isEdit) and $isEdit)
                            <form method="post" action="{{url("edit-items")}}">
                        @else
                            <form method="post" action="{{url("add-items")}}">
                        @endif
                            @csrf
                            @if(isset($isEdit) and $isEdit)
                                <input type="hidden" name="locId" value="{{$id}}">
                            @else
                                <input type="hidden" name="location" value="{{$location}}">
                                <input type="hidden" name="locationType" value="{{$locationType}}">
                                <input type="hidden" name="startDate" value="{{$startDate}}">
                                <input type="hidden" name="endDate" value="{{$endDate}}">
                                <input type="hidden" name="note" value="{{$note}}">
                            @endif
                                @if(isset($isEdit) and $isEdit and isset($selectedItems) and count($selectedItems) > 0)
                                    <table id="old_type_table">
                                        <tr>
                                            <th>
                                                Types sélectionnés
                                            </th>
                                            <th>
                                                Ensemble/Article sélectionnés
                                            </th>
                                            <th>

                                            </th>
                                        </tr>
                                        @foreach($selectedItems as $id => $selectedItem)
                                            <tr>
                                                <td>
                                                    <input type="hidden" name="oldItem[]" value="{{$id}}">
                                                    {{$selectedItem["name"]}}
                                                </td>
                                                <td>
                                                    {{$selectedItem["ensemble"]}}
                                                </td>
                                                <td>
                                                    <button type="button" class="removeButton" onclick="this.parentElement.parentElement.remove();">Retirer</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                    <hr><hr>
                                @endif

                            <table id="type_table">
                                <tr>
                                    <th>
                                        Sélectionner un type
                                        <div style="font-weight: normal">Sélectionner ou taper pour rechercher</div>
                                    </th>
                                </tr>
{{--                                @if(isset($selectedItems))--}}
{{--                                    @php--}}
{{--                                        $count = 0;--}}
{{--                                    @endphp--}}
{{--                                    @foreach($selectedItems as $selectedItem)--}}
{{--                                        <tr id="td_1">--}}
{{--                                            <td>--}}
{{--                                                <label>--}}
{{--                                                    <select class="js-example-basic-single-{{$count++}} pre-selected-item" name="item[]">--}}
{{--                                                        @foreach($itemArray as $key => $value)--}}
{{--                                                            <option value="{{$key}}">{{$value}}</option>--}}
{{--                                                        @endforeach--}}
{{--                                                    </select>--}}
{{--                                                </label>--}}
{{--                                            </td>--}}
{{--                                            <td>--}}
{{--                                                <button type="button" class="removeButton" onclick="removeTr(this);">Enlever</button>--}}
{{--                                            </td>--}}
{{--                                            <td>--}}
{{--                                                None.--}}
{{--                                            </td>--}}
{{--                                        </tr>--}}
{{--                                    @endforeach--}}
{{--                                @else--}}
{{--                                    <tr id="td_1">--}}
{{--                                        <td>--}}
{{--                                            <label>--}}
{{--                                                <select class="js-example-basic-single-1" name="item[]">--}}
{{--                                                    @foreach($itemArray as $key => $value)--}}
{{--                                                        <option value="{{$key}}">{{$value}}</option>--}}
{{--                                                    @endforeach--}}
{{--                                                </select>--}}
{{--                                            </label>--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            <button type="button" class="removeButton" onclick="removeTr(this);">Enlever</button>--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            None.--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}
{{--                                @endif--}}
                                <tr id="final_tr"></tr>
                            </table>

                            <div>
                                <button type="button" class="addButton" onclick="addTr();">Ajouter</button>
                                <div class="buttonHolder" style="display: inline; float: right">
                                    <input class="rentalSubmit" type="submit" value="Enregistrer">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script type="application/javascript">
            var count = 1;
            var options = "@foreach($itemArray as $key => $value)<option value=\"{{$key}}\">{{$value}}</option>@endforeach";

{{--            @if(isset($selectedItems))--}}
{{--                $(document).ready(function() {--}}
{{--                    var preSelectedItemsJSON = @json($selectedItems);--}}
{{--                    $('.pre-selected-item').each(function (i, obj) {--}}
{{--                        $(this).select2();--}}
{{--                        $(this).val(preSelectedItemsJSON[i]);--}}
{{--                    });--}}
{{--                });--}}
{{--            @else--}}
{{--                $(document).ready(function() {--}}
{{--                    $('.js-example-basic-single-1').select2();--}}
{{--                });--}}
{{--            @endif--}}

            function addTr() {
                var table = document.getElementById("type_table");
                var finalTr = document.getElementById("final_tr");
                var newTr = document.createElement("tr");
                newTr.innerHTML = "<td><label>\n" +
                    "                                            <select class=\"js-example-basic-single-"+count+"\" name=\"item[]\">" +
                    options +
                    "</td><td>\n" +
                    "                                        <button type='button' class=\"removeButton\" onclick=\"removeTr(this)\">Retirer</button>\n" +
                    "                                    </td><td>"
                table.insertBefore(newTr, finalTr.nextSibling);
                $('.js-example-basic-single-' + count).select2();

                //Test
                count++;
            }

            function removeTr(btn) {
                var parentTr = btn.parentElement.parentElement;
                parentTr.remove();
            }
        </script>
    </div>
@endsection
