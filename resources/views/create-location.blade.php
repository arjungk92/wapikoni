@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Créer une location</div>

                    <div class="card-body">
                        <form method="post">
                            @csrf
                            <table>
                                <tr>
                                    <td>
                                        Type de prêt
                                    </td>
                                    <td>
                                        <select name="locationType">
                                            <option value="Wapikoni Mobile" {{"Wapikoni Mobile" == old('locationType') ? "selected" : ""}}>
                                                Wapikoni Mobile
                                            </option>
                                            <option value="Prêt externe" {{"Prêt externe" == old('locationType') ? "selected" : ""}}>
                                                Prêt externe
                                            </option>
                                            <option value="Prêt interne" {{"Prêt interne" == old('locationType') ? "selected" : ""}}>
                                                Prêt interne
                                            </option>
                                            <option value="Musique Nomade" {{"Musique Nomade" == old('locationType') ? "selected" : ""}}>
                                                Musique Nomade
                                            </option>
                                            <option value="Vélo Paradiso" {{"Vélo Paradiso" == old('locationType') ? "selected" : ""}}>
                                                Vélo Paradiso
                                            </option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Escale
                                        @error('location')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </td>
                                    <td>
                                        <label>
                                            <input type="text" name="location" value="{{old('location')}}">
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Date de départ
                                        @error('startDate')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </td>
                                    <td>
                                        <label>
                                            <input type="date" name="startDate" value="{{old('startDate')}}">
                                        </label>
                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        Date de retour
                                        @error('endDate')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </td>
                                    <td>
                                        <label>
                                            <input type="date" name="endDate" value="{{old('endDate')}}">
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Notes
                                    </td>
                                    <td>
                                        <label>
                                            <textarea name="note"></textarea>
                                        </label>
                                    </td>
                                </tr>
                            </table>
                            <div class="buttonHolder">
                                <input class="rentalSubmit" type="submit" value="Suivant">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
