@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        <div class="buttonHolder">
                            <a href="{{url('/')}}"><input class="rentalSubmit" type="submit" value="Retourner à l'accueil" style="background-color: #3f83f8"></a>
                        </div>
                        <div class="buttonHolder">
                            <a href="https://airtable.com/login"><input class="rentalSubmit" type="submit" value="Ouvrir Airtable"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
