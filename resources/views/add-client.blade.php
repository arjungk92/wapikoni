@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><b>Choisir un client existant</b></div>

                    <div class="card-body">
                        <form method="post" action="{{url("add-client")}}">
                            @csrf
                            <input type="hidden" name="location" value="{{$location}}">
                            <table id="type_table">
                                <tr id="td_1">
                                    <td>
                                        Sélectionner
                                    </td>
                                    <td>
                                        <label>
                                            <select class="js-example-basic-single" name="client">
                                                @foreach($clients as $key => $value)
                                                    <option value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                        </label>
                                    </td>
                                </tr>
                            </table>

                            <div>
                                <div class="buttonHolder" style="display: inline; float: right">
                                    <input class="rentalSubmit" type="submit" value="Terminer">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <hr>----ou alors----<hr>
                <div class="card">
                    <div class="card-header"><b>Créer un nouveau client</b></div>

                    <div class="card-body" style="width: 600px;">
                        <form method="post" action="{{url("add-client")}}">
                            @csrf
                            <input type="hidden" name="location" value="{{$location}}">
                            <input type="hidden" name="isCreateClient" value="1">
                            <table>
                                <tr>
                                    <td>
                                        Organisme/Entreprise
                                        @error('organization')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </td>
                                    <td>
                                        <label>
                                            <input type="text" name="organization" value="{{old('organization')}}" required>
                                        </label>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        Contact
                                        @error('contact')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </td>
                                    <td>
                                        <label>
                                            <input type="text" name="contact" value="{{old('contact')}}" required>
                                        </label>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        Adresse
                                        @error('address')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </td>
                                    <td>
                                        <label>
                                            <input type="text" name="address" value="{{old('address')}}" style="width: 600px;" required>
                                        </label>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        Téléphone
                                        @error('telephone')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </td>
                                    <td>
                                        <label>
                                            <input type="text" name="telephone" value="{{old('telephone')}}" required>
                                        </label>
                                    </td>
                                </tr>

{{--                                <tr>--}}
{{--                                    <td>--}}
{{--                                        Identifiant--}}
{{--                                        @error('identity')--}}
{{--                                        <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                                        @enderror--}}
{{--                                    </td>--}}
{{--                                    <td>--}}
{{--                                        <label>--}}
{{--                                            <input type="text" name="identity" value="{{old('identity')}}">--}}
{{--                                        </label>--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
                            </table>

                            <div>
                                <div class="buttonHolder" style="display: inline; float: right">
                                    <input class="rentalSubmit" type="submit" value="Terminer">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(document).ready(function() {
                $('.js-example-basic-single').select2();
            });
        </script>
    </div>
@endsection
