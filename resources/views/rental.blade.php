@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div>
                <div class="card" style="width: 1200px;">
                    <div class="card-header">Finalisation de : {{ $rental }}</div>

                    <div class="card-body" style="width: 1200px;">
                        <form method="post">
                            @csrf
                            <table style="width: 900px;">
                                <th style="width: 450px;">
                                    Types
                                </th>
                                <th>
                                    Ensembles et articles uniques
                                </th>
                                @foreach($types as $typeId => $typeArr)
                                    @foreach($typeArr as $id => $type)
                                        <tr>
                                            <td>
                                                <p>{{$type["name"]}}</p>
                                            </td>
                                            <td>
                                                <select name="article[{{$id}}]">
                                                    @if ($type["selected"])
                                                        <option value="" disabled>Veuillez choisir</option>
                                                    @else
                                                        <option value="" disabled selected>Veuillez choisir</option>
                                                    @endif

                                                    @foreach($type["articles"] as $artId => $article)
                                                        @if ($article["selected"])
                                                            <option value="{{$artId}}" selected>{{$article["name"]}}</option>
                                                        @else
                                                            <option value="{{$artId}}">{{$article["name"]}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </table>
                            <div class="buttonHolder">
                                <input class="rentalSubmit" type="submit" value="Terminer">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
