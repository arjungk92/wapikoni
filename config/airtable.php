<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Airtable Key
    |--------------------------------------------------------------------------
    |
    | This value can be found in your Airtable account page:
    | https://airtable.com/account
    |
     */
    'key' => env('AIRTABLE_KEY'),

    /*
    |--------------------------------------------------------------------------
    | Airtable Base
    |--------------------------------------------------------------------------
    |
    | This value can be found once you click on your Base on the API page:
    | https://airtable.com/api
    | https://airtable.com/[BASE_ID]/api/docs#curl/introduction
    |
     */
    'base' => env('AIRTABLE_BASE'),

    /*
    |--------------------------------------------------------------------------
    | Default Airtable Table
    |--------------------------------------------------------------------------
    |
    | This value can be found on the API docs page:
    | https://airtable.com/[BASE_ID]/api/docs#curl/table:tasks
    | The value will be hilighted at the beginning of each table section.
    | Example:
    | Each record in the `Tasks` contains the following fields
    |
     */
    'default' => 'default',

    'tables' => [
        'rental' => [
            'name' => env('AIRTABLE_TABLE_LOCATION', "*Locations*"),
        ],
        'type' => [
            'name' => env('AIRTABLE_TABLE_TYPE', "Types")
        ],
        'article' => [
            'name' => env('AIRTABLE_TABLE_ARTICLE', "Ensembles & Articles")
        ],
        'item' => [
            'name' => env('AIRTABLE_TABLE_DETAIL', "Items détaillés")
        ],
        'client' => [
            'name' => env('AIRTABLE_TABLE_CLIENT', "- Clients")
        ],
        'loc_type' => [
            'name' => env('AIRTABLE_TABLE_LOCATION_TYPE', "- Loc-Types")
        ],
        'loc_ens' => [
            'name' => env('AIRTABLE_TABLE_LOCATION_ENSEMBLE', "-Loc-Ens")
        ]
    ],

    'log_http' => env('AIRTABLE_LOG_HTTP', false),
    'log_http_format' => env('AIRTABLE_LOG_HTTP_FORMAT', '{request} >>> {res_body}'),

    'typecast' => env('AIRTABLE_TYPECAST', false),
];
