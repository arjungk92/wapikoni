<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/location/{id}', 'HomeController@getLocation')->name('rental');
Route::post('/location/{id}', 'HomeController@postLocation');
Route::get('create-location', 'LocationController@index');
Route::post('create-location', 'LocationController@indexType');
Route::post('add-items', 'LocationController@createLocation');
Route::post('add-client', 'LocationController@addClient');
Route::get('location/preparation/edit/{id}', 'LocationController@editPreparation');
Route::post('edit-items', 'LocationController@saveEditPreparation');
Route::get('location-delete/{id}', 'LocationController@deleteLocation');
