<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Airtable;

class LocationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index() {
        return view('create-location');
    }

    public function indexType(Request $request) {
        //Verify the info we have so far
        $validated = Validator::make($request->all(), array(
            'location' => 'required',
            'locationType' => 'required',
            'startDate' => 'required|date',
            'endDate' => 'required|date'
        ));

        if($validated->fails()) {
            return back()->withErrors($validated->getMessageBag())->withInput();
        }

        $items = $this->getItems();
        $itemArr = $this->getItemsArray($items);

        return view('add-items')->with(array(
            "items" => $items,
            "itemArray" => $itemArr,
            "count" => count($itemArr),
            "location" => $request->location,
            "locationType" => $request->locationType,
            "startDate" => $request->startDate,
            "endDate" => $request->endDate,
            "note" => $request->note
        ));
    }

    public function createLocation(Request $request) {
        $validated = Validator::make($request->all(), array(
            'location' => 'required',
            'locationType' => 'required',
            'startDate' => 'required|date',
            'endDate' => 'required|date',
            'item' =>'required|array'
        ));

        if($validated->fails()) {
            return back()->withErrors($validated->getMessageBag())->withInput();
        }

        //Create the location.
        $location = Airtable::table('rental')->create(array(
            "Type de prêt" => $request->locationType,
            "Escale" => $request->location,
            "Début de location" => $request->startDate,
            "Fin de location" => $request->endDate,
            "Types" => $request->item
        ));

        //Create entries into the intermediate location type table.
        $this->addEntriesToLocationType($location["id"], $request->item);

        $clients = Airtable::table('client')->all();
        $clientArr = array();

        foreach ($clients as $client) {
            $clientArr[$client["id"]] = $client["fields"]["Identifiant"];
        }

        return view('add-client')->with(array(
            "location" => $location["id"],
            "clients" => $clientArr
        ));
    }

    public function addClient(Request $request) {
        if(isset($request->isCreateClient)) {
            $validated = Validator::make($request->all(), array(
                'location' => 'required',
                'organization' => 'required',
                'contact' => 'required',
                'address' => 'required',
                'telephone' => 'required',
//                'project' => 'required',
//                'identity' => 'required',
            ));

            if($validated->fails()) {
                return back()->withErrors($validated->getMessageBag())->withInput();
            }

            //Create Client
            $client = Airtable::table('client')->create(array(
                'Organisme/Entreprise' => $request->organization,
                "Contact" => $request->contact,
                "Adresse" => $request->address,
                "Téléphone" => $request->telephone,
                "Locations" => array(
                    $request->location
                )
            ));
        }
        else {
            $validated = Validator::make($request->all(), array(
                'location' => 'required',
                'client' => 'required'
            ));

            if($validated->fails()) {
                return back()->withErrors($validated->getMessageBag())->withInput();
            }

            //Find Client
            $client = Airtable::table('client')->find($request->client);

            //Add this new location to the searched client.
            if(isset($client["fields"]["Locations"]))
                $locationsArr = $client["fields"]["Locations"];
            else
                $locationsArr = array();

            array_push($locationsArr, $request->location);
            $updatedClient = Airtable::table('client')->patch($request->client, ["Locations" => $locationsArr]);
        }

        return view('success');
    }

    public function editPreparation($id, Request $request) {
        if($location = Airtable::table('rental')->find($id)) {

            $types = $this->getPreselectedTypes($id);

            $items = $this->getItems();
            $itemArr = $this->getItemsArray($items);

            return view('add-items')->with(array(
                "rental" => $location["fields"]["Name"],
                "items" => $items,
                "itemArray" => $itemArr,
                "count" => count($itemArr),
                "id" => $id,
                "isEdit" => 1,
                "selectedItems" => $types
            ));
        }
        else {
            return back()->withErrors(array(
                'error' => 'Location does not exist'
            ));
        }
    }

    public function saveEditPreparation(Request $request) {
        $validated = Validator::make($request->all(), array(
            'locId' => 'required'
        ));

        if($validated->fails()) {
            return back()->withErrors($validated->getMessageBag())->withInput();
        }

        if($location = Airtable::table('rental')->find($request->locId)) {
            Airtable::table('rental')->patch($request->locId, ["Types" => $request->item]);

            //Add entries to intermediate loc_type table.
            $this->removeEntriesLocationType($request->locId, isset($request->oldItem) ? $request->oldItem : array());

            if(isset($request->item) and count($request->item) > 0) {
                $this->addEntriesToLocationType($request->locId, $request->item);
            }


            return view('success');
        }
        else {
            return back()->withErrors(array(
                'error' => 'Location does not exist'
            ));
        }
    }

    public function getItems() {
        return Airtable::table('type')->all();
    }

    public function getItemsArray($items) {
        $itemArr = array();
        foreach ($items as $item) {
            if(isset($item["fields"]["Catégorie des items"][0])) {
                $itemArr[$item["id"]] = $item["fields"]["Catégorie des items"][0] . " - " . $item["fields"]["Type"];
            }
        }

        asort($itemArr);
        return $itemArr;
    }

    public function addEntriesToLocationType($location, $types) {
        foreach ($types as $type) {
            $locType = Airtable::table('loc_type')->create(array(
                'Locations liées' => $location,
                'Types liés' => $type
            ));
        }
    }

    public function removeEntriesLocationType($location, $oldItems) {
        //Find all loc type entries with this location id.
        $locTypes = Airtable::table('loc_type')->where('IDLocation Rollup (from Locations liées)', $location)->get();

        foreach ($locTypes["records"] as $locType) {
            if(in_array($locType["id"], $oldItems)) {
                //Do Nothing here.
            }
            else {
                //Remove Ensemble chosen for this.
                if($preEnseble = Airtable::table('loc_ens')
                        ->where('ID Loc-Types Rollup (from - Loc-Types)', $locType["id"])
                        ->get() and count($preEnseble["records"]) > 0) {
                    Airtable::table('loc_ens')->destroy($preEnseble["records"][0]["id"]);
                }
                Airtable::table('loc_type')->destroy($locType["id"]);
            }
        }
    }

    public function getPreselectedTypes($location) {
        $locTypes = Airtable::table('loc_type')->where('IDLocation Rollup (from Locations liées)', $location)->get();

        $preSelectedArray = array();
        foreach ($locTypes["records"] as $locType) {
            $preSelectedArray[$locType["id"]]["typeId"] = $locType["fields"]["Types liés"][0];
            $preSelectedArray[$locType["id"]]["name"] = $locType["fields"]["ID Loc-Types"];

            if($preEnseble = Airtable::table('loc_ens')
                ->where('ID Loc-Types Rollup (from - Loc-Types)', $locType["id"])
                ->get() and count($preEnseble["records"]) > 0) {
                $preSelectedArray[$locType["id"]]["ensemble"] = $preEnseble["records"][0]["fields"]['ID Loc-Ens.'];
            }
            else {
                $preSelectedArray[$locType["id"]]["ensemble"] = "Not Finalized";
            }
        }

        return $preSelectedArray;
    }

    public function deleteLocation($id) {
        Airtable::table('rental')->destroy($id);
        return redirect('/');
    }
}
