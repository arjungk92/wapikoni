<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Airtable;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $rentals = Airtable::table('rental')->all();

        $returnVals = [];

        foreach ($rentals as $rental) {
            if(strtolower(gettype($rental["fields"]["Name"])) == "string") {
                $returnVals[$rental["id"]] = $rental["fields"]["Abrev. Type de prêt"] . '-' .
                    $rental["fields"]["Escale"] . '-' .
                    str_ireplace('-', '/', $rental["fields"]["Début de location"]) . '-' .
                    str_ireplace('-', '/', $rental["fields"]["Fin de location"]);
            }
        }

        arsort($returnVals);

//        dd($returnVals);
        return view('home')
            ->with('rentals', $returnVals);
    }

    public function getLocation(Request $request, string $tId) {
        try {
            $rental = Airtable::table('rental')->find($tId);
        }
        catch (\Exception $e) {
            return abort(404);
        }

        $typeArr = [];

        $typesIDArray = $this->getPreselectedTypes($tId);
        $preSelectedArticleArr = $this->getPreSelectedEnsembles($tId);

        foreach ($typesIDArray as $recordId => $typeId) {
            $preSelected = false;
            try {
                $type = Airtable::table('type')->find($typeId);
            }
            catch (\Exception $e) {
                return abort(404);
            }

            $articleArr = [];

            foreach($type["fields"]["Ensembles/Articles uniques"] as $artId) {
                try {
                    $article = Airtable::table('article')->find($artId);
                }
                catch (\Exception $e) {
                    continue;
                }

                $articleArr[$artId] = array(
                    "name" => $article["fields"]["Ensemble/Article unitaire"],
                    "selected" => isset($preSelectedArticleArr[$recordId]) and $preSelectedArticleArr[$recordId] ==  $artId
                );

                if(isset($preSelectedArticleArr[$recordId]) and $preSelectedArticleArr[$recordId] ==  $artId) {
                    $preSelected = true;
                }
            }

            if(!isset($typeArr[$typeId])) {
                $typeArr[$typeId] = array();
            }

            $typeArr[$typeId][$recordId] = array(
                "name" => $type["fields"]["Type"],
                "articles" => $articleArr,
                "selected" => $preSelected
            );
        }

        return view('rental')
            ->with('types', $typeArr)
            ->with('rental', $rental["fields"]["Name"]);
    }

    public function postLocation(Request $request, string $id) {
        $artArr = $request->article;

        //Remove Old entries to create way for new ones.
        $this->removePreselectedLocationEnsembles($id);

        //Create Loc Ens entries.
        $this->addEntriesToLocationEnsemble($id, $artArr);

        try {
            Airtable::table('rental')->patch($id, ['Ensembles & Articles uniques' => $artArr]);
        }
        catch (\Exception $e) {
            abort(404);
        }
        return view('success');
    }

    public function getPreselectedTypes($location) {
        $locTypes = Airtable::table('loc_type')->where('IDLocation Rollup (from Locations liées)', $location)->get();

        $preSelectedArray = array();
        foreach ($locTypes["records"] as $locType) {
            $preSelectedArray[$locType["id"]] = $locType["fields"]["Types liés"][0];
        }

        return $preSelectedArray;
    }

    public function getPreSelectedEnsembles($location) {
        $locEnss = Airtable::table('loc_ens')->where('IDLocation Rollup (from Locations liées)', $location)->get();

        $preSelectedArray = array();
        foreach ($locEnss["records"] as $locEns) {
            if(isset($locEns["fields"]["ID Loc-Types Rollup (from - Loc-Types)"]) and isset($locEns["fields"]["Ensembles liés"][0])) {
                $preSelectedArray[$locEns["fields"]["ID Loc-Types Rollup (from - Loc-Types)"]] = $locEns["fields"]["Ensembles liés"][0];
            }
        }

        return $preSelectedArray;
    }

    public function removePreselectedLocationEnsembles($location) {
        $locEnss = Airtable::table('loc_ens')->where('IDLocation Rollup (from Locations liées)', $location)->get();

        foreach ($locEnss["records"] as $locEns) {
            Airtable::table('loc_ens')->destroy($locEns["id"]);
        }
    }

    public function addEntriesToLocationEnsemble($location, $typesEnsembleArr) {
        foreach ($typesEnsembleArr as $locTypeId => $artId) {
            $locEns = Airtable::table('loc_ens')->create(array(
                'Locations liées' => $location,
                'Ensembles liés' => $artId,
                '- Loc-Types' => $locTypeId
            ));
        }
    }
}
